# README #
Name: Enhanced_ACRF(Enhanced version of Automatic Configuration of RouteFlow)  

This software automatically generates the virtual environment proposed for RouteFlow. The software is an enhanced version of Automatic Conﬁguration of Routing Control Platforms (https://github.com/routeflow/AutomaticConfigurationRouteFlow) and RouteFlow (https://github.com/CPqD/RouteFlow/).   

This code is contributed by Mengchen SHI - TUB The code is written in the part of work performed in my Master Thesis. For any question or bug, please report at mcshi.tommy@gmail.com.  

Version : 3.0  

### What is this repository for? ###

* For teaching, researching, or other purpose use Routing protocols in OPENFLOW environment
* Create RouteFlow automatically upon discovery any OpenFlow network


### Contributions ###
* Add Full BGP Supported, multiple AS supported
* Decouple the functions of discovery message(xmlagent.py) and VM generation(vmserver.py) so can fit for other Routeflow Version and more controllers.
* Enhanced stability and fewer bugs for VM Generation.
* Cooperating with Topology generator(URL) make anyone can modify the config file easily.
* Make Routing automatic configuration close to usual practice.
* Add the solution without Flowvisor to improve the performance

### Requirement ###
System: Ubuntu 12.04

Others:
sudo apt-get install build-essential git libboost-dev libboost-program-options-dev libboost-thread-dev libboost-filesystem-dev iproute-dev openvswitch-switch python-pymongo lxc ant openjdk-6-jdk python-pexpect python-ipaddr python-dev mongodb python-pip

Mongodb: can be updated to newest version 2.4.14(See documentation how to install specific version)

### What can this repository do?###


### How do I get set up? ###

* Install the dependencies
* Clone this repo
* git clone https://bitbucket.org/superrace/acrf-extended
* cd acrf-extended
* make rfclient
* cd FLOWVISOR
* make
* pip install --upgrade pip
* pip install ryu

### Note ###
1. For some case, reverse path will happend and icmp will be blocked by default firewall rule of unix. So use this command to disable rp_filter in all ports involved in experiment.
sudo sysctl -w "net.ipv4.conf.all.rp_filter=0"
sudo sysctl -w "net.ipv4.conf.default.rp_filter=0"

### How to start ###
* Start the rfauto

		cd acrf-extended/rftest

		sudo ./rfauto

(if you want use the version with FlowVisor and Discovery Controller(RYU), use "sudo ./rfauto -f")

* Start a Mininet script with the IP and port of the EACRF

Port number: 

 With FlowVisor: 6600

 Without FlowVisor: 6633